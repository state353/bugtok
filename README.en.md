# bugtok

#### Description
计划重做一个bugfree系统，并参考redmine
bugfree是上古程序，加载慢处理慢，有些操作还很蹩脚，但是又特别喜欢bugfree的简洁的风格
所以计划自己做一套，如果有啥建议尽管提，我尽量抽空调整
但系统架构我是不会改了，使用我自己最喜欢的一套框架，开发起来顺手

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
